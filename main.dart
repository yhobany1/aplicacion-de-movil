import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sena/ui/splash/splash_screen.dart';
import 'package:sena/bloc/login/login_logic.dart';
import 'package:sena/provider/respuestaApi/respuesta_api_bloc.dart';
import 'package:sena/bloc/login/login_bloc.dart';

import 'bloc/login/login_bloc.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(create: (context){
            return LoginBloc(logic: LoginLogicInit());
          }),
          BlocProvider(create: (context){
            return RespuestaApiBloc();
          }),
        ],
        child: GestureDetector(
        onTap: (){
      FocusScope.of(context).requestFocus(new FocusNode());
    },
    child: MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'D',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
    ),
        ),
    );
  }
}

